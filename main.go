package main

import (
	"github.com/juju/errors"
	"gitlab.com/go-yp/grpc-recipes/components/server"
	"gitlab.com/go-yp/grpc-recipes/models/protos/services/recipes"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	lis, err := net.Listen("tcp", ":32625")
	if err != nil {
		log.Fatalf("[CRITICAL] failed to listen: %+v", errors.Trace(err))
	}
	defer lis.Close()

	s := grpc.NewServer()

	recipes.RegisterRecipesServiceServer(
		s,
		new(server.Server),
	)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("[CRITICAL] Server exited with error: %+v", errors.Trace(err))
	}
}
