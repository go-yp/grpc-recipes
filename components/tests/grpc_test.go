package tests

import (
	"context"
	"github.com/golang/protobuf/proto"
	"github.com/google/go-cmp/cmp"
	"github.com/juju/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-yp/grpc-recipes/components/server"
	"gitlab.com/go-yp/grpc-recipes/models/protos/services/recipes"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"testing"
)

const (
	bufferSize = 1024 * 1024
)

func TestStoreAndFindByIngredients(t *testing.T) {
	connection, err := mockServerConnect(context.Background())
	if !assert.NoError(t, err) {
		return
	}
	defer connection.Close()

	client := recipes.NewRecipesServiceClient(connection)

	recipe1 := &recipes.Recipe{
		Code:  10001,
		Title: "Борщ",
		Ingredients: []*recipes.Ingredient{
			{
				Code: 625,
				Name: "Буряк",
			},
			{
				Code: 725,
				Name: "Квасоля",
			},
			{
				Code: 675,
				Name: "Помідори",
			},
		},
	}

	recipe2 := &recipes.Recipe{
		Code:  10002,
		Title: "Вінегрет з печерицями",
		Ingredients: []*recipes.Ingredient{
			{
				Code: 625,
				Name: "Буряк",
			},
			{
				Code: 825,
				Name: "Печериці",
			},
		},
	}

	mainRecipes := &recipes.Recipes{
		Recipes: []*recipes.Recipe{
			recipe1,
			recipe2,
		},
	}

	storeResponse, err := client.Store(context.Background(), mainRecipes)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, &recipes.Empty{}, storeResponse)

	recipesBy625, err := client.FindByIngredients(context.Background(), &recipes.IngredientsFilter{
		Codes: []uint32{625},
	})
	if !assert.NoError(t, err) {
		return
	}

	diff := cmp.Diff(
		mainRecipes.Recipes,
		recipesBy625.Recipes,
		cmp.Comparer(proto.Equal),
	)

	assert.Equal(t, "", diff)
}

func mockServerConnect(ctx context.Context) (conn *grpc.ClientConn, err error) {
	lis := bufconn.Listen(bufferSize)
	s := grpc.NewServer()

	recipes.RegisterRecipesServiceServer(
		s,
		new(server.Server),
	)

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("[CRITICAL] Server exited with error: %+v", errors.Trace(err))
		}
	}()

	return grpc.DialContext(
		ctx,
		"bufnet",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return lis.Dial()
		}),
		grpc.WithInsecure(),
	)
}

func localhostServerConnect(address string) (conn *grpc.ClientConn, err error) {
	return grpc.Dial(address, grpc.WithInsecure())
}
