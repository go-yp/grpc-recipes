package server

import (
	"context"
	"gitlab.com/go-yp/grpc-recipes/models/protos/services/recipes"
	"sync"
)

var (
	empty = &recipes.Empty{}
)

type Server struct {
	mu   sync.RWMutex
	data []*recipes.Recipe
}

func (s *Server) Store(ctx context.Context, recipes *recipes.Recipes) (*recipes.Empty, error) {
	s.mu.Lock()
	s.data = append(s.data, recipes.Recipes...)
	s.mu.Unlock()

	return empty, nil
}

func (s *Server) FindByIngredients(ctx context.Context, filter *recipes.IngredientsFilter) (*recipes.Recipes, error) {
	result := make([]*recipes.Recipe, 0)

	s.mu.RLock()
	data := s.data
	s.mu.RUnlock()

	codeMap := make(map[uint32]bool, len(filter.Codes))
	for _, code := range filter.Codes {
		codeMap[code] = true
	}

	for _, recipe := range data {
		for _, ingredient := range recipe.Ingredients {
			if codeMap[ingredient.Code] {
				result = append(result, recipe)

				break
			}
		}
	}

	return &recipes.Recipes{
		Recipes: result,
	}, nil
}
