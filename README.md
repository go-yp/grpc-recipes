# grpc-recipes

### Development
##### Dependencies
```bash
dep ensure
```

##### Tests
```text
go test ./components/...
```